package io.github.klsmith.library.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import spark.ResponseTransformer;

public final class JsonUtil {

	private static Gson gson = new GsonBuilder()//
			.setPrettyPrinting()//
			.create();

	private JsonUtil() {
		// Utility classes should not be instantiated.
	}

	public static String toJson(Object object) {
		return gson.toJson(object);
	}

	public static ResponseTransformer json() {
		return JsonUtil::toJson;
	}
}