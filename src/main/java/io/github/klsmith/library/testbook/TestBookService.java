package io.github.klsmith.library.testbook;

import static io.github.klsmith.library.util.JsonUtil.json;
import static spark.Spark.*;

import io.github.klsmith.library.Database;

public final class TestBookService {

	private TestBookService() {
		// Utility classes should not be instantiated.
	}

	public static void withDatabase(Database database) {
		withDAO(TestBookDAO.withDatabase(database));
	}

	public static void withDAO(TestBookDAO dao) {
		/* GET Requests */
		get("/api/v1/testbooks", dao::getAllTestBooks, json());
		get("/api/v1/testbooks/:id", dao::getBookWithId, json());

		/* POST Requests */
		post("/api/v1/testbooks", dao::createBook, json());

		/* DELETE Requests */
		delete("/api/v1/testbooks/:id", dao::deleteBook, json());
	}

}
