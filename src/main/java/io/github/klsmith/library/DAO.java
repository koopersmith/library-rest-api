package io.github.klsmith.library;

public interface DAO {

	public String getTableName();

	public void createTable();

}
