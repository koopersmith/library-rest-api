package io.github.klsmith.library;

import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

import io.github.klsmith.library.testbook.TestBookDAO;
import io.github.klsmith.library.util.ApplicationProperties;

public class Database {

	private final String name;
	private final Sql2o accessor;

	private static final Logger logger = LogManager.getLogger(Database.class);

	private Database(String name, Sql2o accessor) {
		this.name = requireNonNull(name);
		this.accessor = requireNonNull(accessor);
	}

	public static Database establishConnection(ApplicationProperties properties) {
		requireNonNull(properties);
		logger.debug("Establishing database connection...");
		final String databaseUrl = properties.getDatabaseUrl();
		final String databaseUser = properties.getDatabaseUser();
		final String databasePassword = properties.getDatabasePassword();
		logger.debug("Connected to database at '{}' as user '{}'", databaseUrl, databaseUser);
		final Sql2o accessor = new Sql2o(databaseUrl, databaseUser, databasePassword);
		final Database database = new Database(properties.getDatabaseName(), accessor);
		handleBootstrap(database, properties);
		return database;
	}

	private static void handleBootstrap(Database database, ApplicationProperties properties) {
		if (isNull(database)) {
			logger.error("Null Database, skipping bootstrap!");
			return;
		}
		if (isNull(properties)) {
			logger.error("Null ApplicationProperties, skipping bootsrap!");
			return;
		}
		logger.debug("Checking database bootstrap property...");
		if (properties.shouldDoDatabaseBootstrap()) {
			logger.info("Running bootstrap on all tables...");
			DAO[] daoArray = { TestBookDAO.withDatabase(database) };
			DatabaseSchemaDAO schema = DatabaseSchemaDAO.withDatabase(database);
			for (DAO dao : daoArray) {
				final String tableName = dao.getTableName();
				if (!schema.doesTableExistInDatabase(tableName)) {
					logger.debug("Table {} does not exist, creating table...", tableName);
					dao.createTable();
					logger.debug("Done creating table {}", tableName);
				} else {
					logger.debug("Table {} already exists, skipping", tableName);
				}
			}
			logger.info("Bootstrap complete!");
		} else {
			logger.debug("Database bootstrap property is false, skipping...");
		}
	}

	public String getName() {
		return name;
	}

	public Sql2o getAccessor() {
		return accessor;
	}

	public Connection openConnection() {
		return getAccessor().open();
	}
}
