package io.github.klsmith.library;

import static spark.Spark.port;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import io.github.klsmith.library.testbook.TestBookService;
import io.github.klsmith.library.util.ApplicationProperties;
import spark.Spark;

public class LibraryApp {

	private static final Logger logger = LogManager.getLogger(LibraryApp.class);

	public static void main(String[] args) {
		logger.info("Application Startup");

		// Load the properties file.
		final ApplicationProperties properties = ApplicationProperties.load();

		// Establish the database connection.
		final Database database = Database.establishConnection(properties);

		// Establish which port the app will run on
		final int appPort = properties.getAppPort();
		logger.debug("Running app on port {}", appPort);
		port(appPort);

		Spark.exception(Exception.class, (exception, request, response) -> {
			logger.catching(exception);
		});
		// Set up service(s) with database connection.
		logger.info("Setting up service routes...");
		TestBookService.withDatabase(database);
		logger.info("Application Startup Complete");
	}
}
