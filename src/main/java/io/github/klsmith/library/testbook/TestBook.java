package io.github.klsmith.library.testbook;

import java.sql.Date;

class TestBook {
    public int id;
    public String title;
    public String author;
    public Date releaseDate;
}
