# library-rest-api

A simple proof-of-concept server application written in [Java](https://www.java.com/) (transitioning to [Kotlin](https://kotlinlang.org/)); using [SparkJava](http://sparkjava.com/) (not to be confused with Apache Spark), [Sql2o](http://www.sql2o.org/), [MariaDB](https://mariadb.com/), [SLF4J](https://www.slf4j.org/), and [Log4j2](https://www.slf4j.org/).


You will need to set up your own database and connection properties in order to run the server yourself. 

You can set the database connection properties by creating **/config/library-app.properties** on your machine. If you are running on Windows **/config/** will resolve to **C:\\\\config\\** or whatever drive the server is running on.

<br/>
<br/>

## Example library-app.properties for a hypothetical MariaDB database
**Notice the escaped colons "\\:"**
```INI
# Removed the database_url config so that the individual parts could be referenced when needed.
# database_url=jdbc\:mysql\://192.168.1.15\:3306/library

# Required
database_location=192.168.1.15
database_port=3306
database_name=library
database_user=library_user
database_password=LibraryUserPassword123!

# Optional (if unspecified, they will default to the value shown)
database_bootstrap=false # SEE WARNING!
app_port=4567

# WARNING, if true, it will automatically create tables that don't exist!
```

<br/>
<br/>

## REST Endpoints
| HTTP Method | Endpoint                     | Description                                      |
| ---         | ---                          | ---                                              |
| GET         | ```/api/v1/testbooks```      | Returns a list of all test books.                |
| GET         | ```/api/v1/testbooks/{id}``` | Returns test book with matching id.              |
| POST        | ```/api/v1/testbooks```      | Pass json in the body to create a new test book. |
