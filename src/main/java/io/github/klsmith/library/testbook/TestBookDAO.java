package io.github.klsmith.library.testbook;

import java.util.ArrayList;
import java.util.List;
import static java.util.Objects.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.http.HttpStatus;
import org.sql2o.Connection;

import com.google.gson.Gson;

import io.github.klsmith.library.DAO;
import io.github.klsmith.library.Database;
import spark.Request;
import spark.Response;

public class TestBookDAO implements DAO {

	private static final String TABLE_NAME = "TESTBOOK";
	private static final int NULL_ID = 0;

	private static final Logger logger = LogManager.getLogger(TestBookDAO.class);

	private final Database database;
	private final Gson gson;

	private TestBookDAO(Database database) {
		this.database = requireNonNull(database);
		gson = new Gson();
	}

	public static TestBookDAO withDatabase(Database database) {
		return new TestBookDAO(database);
	}

	public String getTableName() {
		return TABLE_NAME;
	}

	public void createTable() {
		final String sql = "CREATE TABLE TESTBOOK"//
				+ " (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY," //
				+ " title VARCHAR(255) NOT NULL," //
				+ " author VARCHAR(255) NOT NULL," //
				+ " releaseDate DATE NOT NULL);";
		try (Connection connection = database.openConnection()) {
			connection.createQuery(sql) //
					.addParameter("tableName", getTableName()) //
					.executeUpdate();
		}
	}

	public List<TestBook> getAllTestBooks(Request request, Response response) {
		final String sql = "SELECT * FROM TESTBOOK";
		List<TestBook> testBooks = new ArrayList<>();
		try (Connection connection = database.openConnection()) {
			testBooks = connection.createQuery(sql) //
					.executeAndFetch(TestBook.class);
		}
		return testBooks;
	}

	public TestBook getBookWithId(Request request, Response response) {
		final int id = Integer.parseInt(request.params("id"));
		final String sql = "SELECT * FROM TESTBOOK WHERE id=:idParam";
		TestBook testBook = null;
		try (Connection connection = database.openConnection()) {
			testBook = connection.createQuery(sql) //
					.addParameter("idParam", id) //
					.executeAndFetchFirst(TestBook.class);
			connection.close();
		}
		return testBook;
	}

	public int createBook(Request request, Response response) {
		final TestBook newTestBook = gson.fromJson(request.body(), TestBook.class);
		logger.info("Request to create TestBook {}", newTestBook);
		Integer newId = Integer.valueOf(NULL_ID);
		final String sql = "INSERT INTO TESTBOOK (title, author, releaseDate) VALUES (:titleParam, :authorParam, :releaseDateParam)";
		try (Connection connection = database.openConnection()) {
			newId = connection.createQuery(sql) //
					.addParameter("titleParam", newTestBook.title) //
					.addParameter("authorParam", newTestBook.author) //
					.addParameter("releaseDateParam", newTestBook.releaseDate) //
					.executeUpdate() //
					.getKey(Integer.class);
			response.status(HttpStatus.CREATED_201);
		}
		return newId.intValue();
	}

	public int deleteBook(Request request, Response response) {
		final int idToDelete = Integer.parseInt(request.params("id"));
		logger.info("Request to delete TestBook with id {}", idToDelete);
		final String sql = "DELETE FROM TESTBOOK WHERE id=:idParam";
		Integer deletedId = Integer.valueOf(NULL_ID);
		try (Connection connection = database.openConnection()) {
			connection.createQuery(sql) //
					.addParameter("idParam", idToDelete) //
					.executeUpdate();
			response.status(HttpStatus.NO_CONTENT_204);
		}
		return deletedId.intValue();
	}
}
