package io.github.klsmith.library;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sql2o.Connection;

public class DatabaseSchemaDAO implements DAO {

	private static final String TABLE_NAME = "information_schema.TABLES";
	private static final Logger logger = LogManager.getLogger(DatabaseSchemaDAO.class);

	private Database database;

	private DatabaseSchemaDAO(Database database) {
		this.database = database;
	}

	public static DatabaseSchemaDAO withDatabase(Database database) {
		return new DatabaseSchemaDAO(database);
	}

	public String getTableName() {
		return TABLE_NAME;
	}

	@Deprecated
	public void createTable() {
		logger.warn("This class cannot create its own table, do not call this method.");
	}

	public boolean doesTableExistInDatabase(String tableName) {
		final String sql = "SELECT count(*)" //
				+ " FROM " + getTableName() //
				+ " WHERE (TABLE_SCHEMA = :databaseNameParam)" //
				+ " AND (TABLE_NAME = :tableNameParam)";
		boolean result = false;
		try (Connection connection = database.openConnection()) {
			Integer count = connection.createQuery(sql) //
					.addParameter("databaseNameParam", database.getName()) //
					.addParameter("tableNameParam", tableName) //
					.executeScalar(Integer.class);
			result = count.intValue() >= 1;
		}
		return result;
	}
}
