package io.github.klsmith.library.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ApplicationProperties {

	private static final String PROPERTIES_LOCATION = "/config/library-app.properties";
	private static final String DATABASE_LOCATION_KEY = "database_location";
	private static final String DATABASE_PORT_KEY = "database_port";
	private static final String DATABASE_NAME_KEY = "database_name";
	private static final String DATABASE_USER_KEY = "database_user";
	private static final String DATABASE_PASSWORD_KEY = "database_password";
	private static final String DATABASE_BOOTSTRAP_KEY = "database_bootstrap";
	private static final String APP_PORT_KEY = "app_port";
	private static final String JDBC_DATABASE_URL_TEMPLATE = "jdbc:mysql://" + DATABASE_LOCATION_KEY + ":"
			+ DATABASE_PORT_KEY + "/" + DATABASE_NAME_KEY;

	private static final int DEFAULT_APP_PORT = 4567;

	private Properties properties;

	private ApplicationProperties(Properties properties) {
		this.properties = properties;
	}

	public static ApplicationProperties load() {
		Properties properties = new Properties();
		InputStream input = null;
		try {
			input = new FileInputStream(PROPERTIES_LOCATION);
			properties.load(input);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return new ApplicationProperties(properties);
	}

	public String getDatabaseUser() {
		return properties.getProperty(DATABASE_USER_KEY);
	}

	public String getDatabasePassword() {
		return properties.getProperty(DATABASE_PASSWORD_KEY);
	}

	public String getDatabaseUrl() {
		return JDBC_DATABASE_URL_TEMPLATE//
				.replace(DATABASE_LOCATION_KEY, getDatabaseLocation())//
				.replace(DATABASE_PORT_KEY, getDatabasePort())//
				.replace(DATABASE_NAME_KEY, getDatabaseName());
	}

	public String getDatabaseLocation() {
		return properties.getProperty(DATABASE_LOCATION_KEY);
	}

	public String getDatabasePort() {
		return properties.getProperty(DATABASE_PORT_KEY);
	}

	public String getDatabaseName() {
		return properties.getProperty(DATABASE_NAME_KEY);
	}

	public boolean shouldDoDatabaseBootstrap() {
		return Boolean.parseBoolean(properties.getProperty(DATABASE_BOOTSTRAP_KEY));
	}

	public int getAppPort() {
		final String port = properties.getProperty(APP_PORT_KEY);
		if (port != null) {
			try {
				return Integer.parseInt(port);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}
		return DEFAULT_APP_PORT;
	}
}
